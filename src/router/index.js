import Vue from "vue";
import VueRouter from "vue-router";
import Headlines from "../views/Headlines.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "headlines",
    component: Headlines
  },
  {
    path: "/details",
    name: "details",
    props: true,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/headlines/Details.vue")
  }
];

const router = new VueRouter({
  path: '/',
  mode: 'history',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes
});

export default router;
