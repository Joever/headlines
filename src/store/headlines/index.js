import _each from "lodash/each";
import _filter from "lodash/filter";
import _find from "lodash/find";
import _includes from "lodash/includes";
import * as httpRequest from "../../data/headlines";
import mockHeadlines from "../../data/mock/headlines.json";
import moment from "moment";

export default {
  namespaced: true,
  state: {
    headlines: {
      originalArts: [],
      articles: [],
      totalResults: 0
    }
  },
  getters: {
    getHeadlines: state => state.headlines
  },
  mutations: {
    filterHeadlines: function(state, filters) {
      state.headlines.totalResults = 0;
      const filteredHl = _filter(state.headlines.originalArts, item => {
        const result = _find(filters, searchKey => {
          if (searchKey.name === item.source.name) {
            state.headlines.totalResults += 1;
            return item;
          }
        });

        if (result && result.isCheck) return item;
      });

      state.headlines.articles = [...filteredHl];
    },

    headlines: function(state, payload) {
      state.headlines.articles = [];
      state.headlines.totalResults = 0;
      state.headlines.totalResults = payload.totalResults;

      _each(payload.articles, item => {
        item.urlToImage = !item.urlToImage
          ? require("../../assets/white-image.jpg")
          : item.urlToImage;
        state.headlines.articles.push({
          isReadMore: false,
          descLength: 40,
          ...item
        });
      });

      state.headlines.originalArts = state.headlines.articles;
    }
  },
  actions: {
    getHeadlines: function({ commit, dispatch }) {
      const from = moment().format("YYYY-MM-DD");
      httpRequest
        .getHeadlines(from)
        .then(res => {
          commit("headlines", res.data);
          dispatch("sources/getSources", null, { root: true });
        })
        .catch(() => {
          // Developer accounts are limited to 500 requests over a 24 hour period (250 requests available every 12 hours).
          // Use this mock headlines to test.
          commit("headlines", {
            articles: mockHeadlines,
            totalResults: 4
          });
        });
    },

    getSearchHeadlines: function({ commit }, searchKey) {
      httpRequest
        .getSearchHeadlines(searchKey)
        .then(res => {
          commit("headlines", res.data);
        })
        .catch(() => {
          //Clear headlines if got an error response
          commit("headlines", []);
        });
    }
  }
};
