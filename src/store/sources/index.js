import _each from "lodash/each";
import _filter from "lodash/filter";
import mockHeadlines from "../../data/mock/sources.json";
import * as httpRequest from "../../data/sources";

export default {
  namespaced: true,
  state: {
    sources: []
  },
  getters: {
    getSources: state => state.sources,
    getSelecterSources: state =>
      _filter(state.sources, source => {
        return source.isCheck;
      })
  },
  mutations: {
    sources: function(state, payload) {
      state.sources = [];
      _each(payload.sources, item => {
        state.sources.push({
          isCheck: true,
          ...item
        });
      });
    },
    checkUncheckSources: function(state, isCheck) {
      const sources = state.sources;
      state.sources = [];
      _each(sources, item => {
        item.isCheck = isCheck;
        state.sources.push({
          ...item
        });
      });
    }
  },
  actions: {
    getSources: function({ commit }) {
      httpRequest
        .getSources()
        .then(res => {
          commit("sources", res.data);
        })
        .catch(() => {
          // Developer accounts are limited to 500 requests over a 24 hour period (250 requests available every 12 hours).
          // Use this mock sources to test.
          commit("sources", mockHeadlines);
        });
    }
  }
};
