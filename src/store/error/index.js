import * as httpRequest from "../../data/error";

export default {
  namespaced: true,
  actions: {
    getErrorRequest: function() {
      return httpRequest.errorRequest();
    }
  }
};
