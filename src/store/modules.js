import headlines from "./headlines";
import sources from './sources';
import history from './history';
import error from './error';

export default {
  headlines,
  sources,
  history,
  error
};
