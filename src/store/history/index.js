export default {
  namespaced: true,
  getters: {
    getHistory: () => localStorage.getItem("userHistory")
  }
};
