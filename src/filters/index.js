import Vue from "vue";
import moment from 'moment';

Vue.filter("date-YYMMDD", val => {
  if (!val) return;
  return moment(val).format("YY.MM.DD");
});

Vue.filter("date-MMMDDYYY", val => {
    if (!val) return;
    return moment(val).format("MMM DD YYYY");
  });