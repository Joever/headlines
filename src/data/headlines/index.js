import axios from "axios";
import config from "../../config";

export function getHeadlines(from) {
  return axios.get(
    `https://newsapi.org/v2/everything?q=bitcoin&from=${from}&sortBy=publishedAt&apiKey=${config.API_KEY}`
  );
}

export function getSearchHeadlines(searchKey) {
  return axios.get(
    `https://newsapi.org/v2/top-headlines?q=${searchKey}&apiKey=${config.API_KEY}`
  );
}
