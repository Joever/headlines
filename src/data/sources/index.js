import axios from "axios";
import config from "../../config";

export function getSources() {
  return axios.get(`https://newsapi.org/v2/sources?apiKey=${config.API_KEY}`);
}
